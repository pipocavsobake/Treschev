\section{Hill's formula}

Let us add some notation to discrete Lagrangian systems. Non-degeneracy condition means the matrix of second derivatives
\[
  \det \frac{d^2l(x_-, x)}{\partial x_-\partial x} \ne 0\pau (m\times m).
\]

Our main example $M = S^1$.

Trajectory $x_k\in M$ is a sequence such that
\begin{equation}\label{tdef}
  \forall\ j\in\Z\pau (x_j, x_{j+1}) = T(x_{j-1}, x_j).
\end{equation}

By definition $T$ is such that \eqref{tdef} is equivalent to \eqref{dls-dynamical}.

We want to express $x_{j+1}$ as a function of a $x_{j-1}$ and $x_{j}$. That's why
\[
  \det\frac{\partial^2 l(x_-, x)}{\partial x_-\partial x} \ne 0.
\]
The implicit function theorem is a local theorem.

Out main aim is the Hill's formula for periodic trajectories.

Let $(x_1, \ldots, x_n)$ be a periodic trajectory of a DLS. This means that
\begin{multline*}
  T(x_1,x_2) = (x_2,x_3), \ldots, T(x_{n-2}, x_{n-1}) = (x_{n-1}, x_n),\\
  T(x_{n-1}, x_n) = (x_n, x_1),
  T(x_n, x_1) = (x_1, x_2).
\end{multline*}

It is convenient to assume that $x_{n+1} = x_1$, $x_0 = x_n$.
Also we can wright Lagrangian definition. We will discuss only variational definition. Let $x = (x_1,\ldots, x_n)$. Consider variation $x^\e = (x_1^\e,\dots, x_n^\e)$ such that boundary conditions do not need.

Again $x$ is a periodic trajectory if
\[
  \frac{d}{d\e}\bigg|_{\e = 0} S(x^\e) = 0,
\]
where $S$ is action. But we have another definition of the action. Now $S$ is a periodic action if
\[
  S(x) = \sum\limits_{j=1}^nl(x_j, x_{j+1}),
\]
where $x_{n+1} = x_1$.

Any point is fixed for Poincare map $T^n\colon T^n(x_n, x_1) = (x_n, x_1)$.

We have the monodromy matrix
\[
  P = DT^n(x_n, x_1).
\]

Beginning from this time moment we assume that $m=1$. For now $P$ is $2\times 2$ constant matrix.

We can consider also another object. The Hessian matrix. Let $x$ be the critical point of $S$. Then
\[
  H = \frac{\partial^2 S}{\partial x^2}.
\]
It is $(n\times n)$ symmetric matrix.

A question is does some relation between these two objects, $P$ and $H$, exist.

$P$ has information about linearisation of dynamical system. $H$ gives us information about critical point as extremal points. So $P$ tells us about dynamics and $H$ tells about geometry.

Notation:
\[
  \frac{\partial l(x_j, x_{j+1})}{\partial x_j\partial x_{j+1}}(x_j, x_{j+1}) = - b_j.
\]

And the Hill's formula
\[
  \det(P-I) = -\frac{\det H}{b_1 b_2\dots b_n},
\]
where $I$ is an identity operator (unit matrix).

\begin{Proof}
  Notice that $T^n = T\circ \dots \circ T$ and we can consider decomposition of $P = P_n\dots P_1$, where
  \[
    P_1 = \frac{\partial(x_1, x_2)}{\partial(x_n, x_1)},\ 
    P_2 = \frac{\partial(x_2, x_3)}{\partial (x_1, x_2)}.
  \]
  For $P_2$ there $x_3 = x_3(x_1, x_2)$.
  \[
    P_k = \frac{\partial (x_k, x_{k+1})}{\partial (x_{k-1}, x_k)} =
    \begin{pmatrix}
      0 & 1\\
      \frac{\partial{x_{k+1}}}{x_{k-1}} & \frac{x_{k+1}}{x_k}
    \end{pmatrix}
  \]

  We have equations
  \[
    \frac{\partial}{\partial x_k}\big(l(x_{k-1}, x_k) + l(x_k, x_{k+1})\big) = 0.
  \]

  Let us differentiate them (assuming that \(x_{k-1}, x_k\) are independent variables).
  \[
    \frac{\partial^2}{\partial x_k\partial x_{k-1}} l(x_{k-1}, x_k) +
    \frac{\partial^2}{\partial x_k\partial x_{k+1}} l(x_k, x_{k+1})
  \cdot \frac{\partial x_{k+1}}{\partial x_{k-1}} = 0.
  \]
  In our notation $-b_{k-1}  - b_k\frac{\partial x_{k+1}}{\partial x_{k-1}} = 0$. Then $\frac{\partial x_{k+1}}{\partial x_{k-1}} = -\frac{b_{k-1}}{b_k}$.

  Consider another derivative.
  \[
    \frac{\partial^2 l}{\partial x_k^2}(x_{k-1}, x_k) +
    \frac{\partial^2 l}{\partial x_k^2}(x_k, x_{k+1}) +
    \frac{\partial^2 l}{\partial x_k\partial x_{k+1}}\cdot \frac{\partial x_{k+1}}{\partial x_k} = 0.
  \]

  Notice that if we consider \(x_1,\ldots, x_n\) as independent, we have
  \[
    \frac{\partial^2}{x_k^2}\big( l(x_{k-1}, x_k) + l(x_k, x_{k+1})\big) = \frac{\partial^2 S}{x_k^2} =: a_k .
  \]
  With this notation
  \[
    \frac{\partial x_{k+1}}{\partial x_k} = \frac{a_k}{b_k}.
  \]

  So we have a formula for $P_k$
  \[
    P_k = \begin{pmatrix}
      0 & 1\\
      -\frac{b_{k-1}}{b_k} & \frac{a_k}{b_k}
    \end{pmatrix}.
  \]
  According to that $\det P_k = \frac{b_{k-1}}{b_k}$, $\det P = 1$. And for $n\ge 3$
  \[
    H = \frac{\partial^2 S}{\partial x^2} =
    \begin{pmatrix}
      a_1  & -b_1 &      &        & -b_n \\
      -b_1 & a_2  & -b_2               \\
           & -b_2 & a_3                \\
           &      &      & \ddots & -b_{n-1} \\
           -b_n &      &      & -b_{n-1} & a_n
    \end{pmatrix}.
  \]
  For $n = 2$
  \[
    H = \begin{pmatrix}
      a_1 & -(b_1 + b_2) \\
      - (b_1 + b_2) & a_2
    \end{pmatrix},
  \]
  because
  \[
    \frac{\partial^2S}{\partial x_1\partial x_2} =
    \frac{\partial^2}{\partial x_1\partial x_2}\big(
      l(x_1, x_2) + l(x_2, x_1)
    \big) =
    \frac{\partial}{\partial x_2}\left(
      \frac{\partial l(x_1, x_2)}{\partial x_1} +
      \frac{\partial l(x_2, x_1)}{\partial x_1}
    \right)
  \].

  Let us prove more general formula
  \[
    \det (P - \rho I) = -\rho \frac{\det H_\rho}{b_1\dots b_n},
  \]
  where
  \[
    H_\rho = \begin{pmatrix}
      a_1  & -b_1 &      &        & -b_n \rho^{-1} \\
      -b_1 & a_2  & -b_2               \\
           & -b_2 & a_3                \\
           &      &      & \ddots & -b_{n-1} \\
           -b_n \rho &      &      & -b_{n-1} & a_n
    \end{pmatrix}
  \]

  The formula to prove is an equality of two polynoms. Let us prove that they have the same roots.

  Statement $\det H_\rho = 0$ is equivalent to $\exists\ v = (v_1, \ldots, v_n)^T$ such that $H_\rho v = 0$. What does it mean
  \begin{eqnarray*}
    a_1 v_1 - b_1 v_2 - b_n v_n \rho ^{-1} &=&  0;\\
    -b_{k-1} v_{k-1} + a_k v_k - b_k v_{k+1} &=& 0;\\
    -\rho b_n v_1 - b_{n-1} v_{n-1} + a_n v_n &=&  0.
  \end{eqnarray*}
  Let's divide by $b_k$
  \begin{eqnarray*}
    \frac{a_1}{b_1}v_1 - v_2 - \frac{b_n}{b_1}\rho^{-1} v_n &=& 0;\\
    -\frac{b_{k-1}}{b_k}v_{k-1} + \frac{a_k}{b_k}v_k - v_{k+1} &=& 0;\\
    -\rho v_1 - \frac{b_{n-1}}{b_n}v_{n-1} + \frac{a_n}{b_n}v_n &=& 0.
  \end{eqnarray*}

  Notice that we can now obtain formulas
  \[
    P_1 \begin{pmatrix}
      \rho^{-1} v_n \\ v_1
    \end{pmatrix} =  \begin{pmatrix}
      v_1 \\ v_2
    \end{pmatrix};\quad
    P_k \begin{pmatrix}
      v_{k-1} \\ v_k
    \end{pmatrix} =  \begin{pmatrix}
      v_k \\ v_{k+1}
    \end{pmatrix};\quad
    P_n \begin{pmatrix}
      v_{n-1} \\ v_n
    \end{pmatrix} =  \begin{pmatrix}
      v_n \\ \rho v_1
    \end{pmatrix}.
  \]

  That means
  \[
    P
    \begin{pmatrix}
      \rho^{-1} v_n \\
      v_1
    \end{pmatrix} = \rho
    \begin{pmatrix}
      \rho^{-1}v_n \\ v_1
    \end{pmatrix}.
  \]

  So $\rho$ is an eigenvalue of $P$. All transformations were equivalent. So roots of $\det(P-\rho I)$ and $\det H_\rho$ are the same.

  Now we must check the equality of the highest coefficients.
  \[
    \det (P-\rho I) = (-1)^n\rho^2 + \ldots.
  \]
  Let's evaluate $\det H_\rho$. Terms must contain $\rho$, hence they contain $-b_n \rho$ as multiplier. We can multiply it only by $-b_{n-1}$ from the last column because $-b_n \rho^{-1}$ contains $\rho^{-1}$. Then if we consider column $n-1$ we can see that we can only use $-b_{n-2}$ e.\,t.\,c. So there is only one term and it equals $(-1)^n b_1 \dots b_n \rho$.
\end{Proof}

