\section{Hamiltonian equations}
In books they are always in canonical coordinates. What are they in arbitrary coordinates?

At first we should start with the phase space $M$ (smooth manifold) and differential $2$-form $\omega$ on $M$.

\begin{Definition}
  $\omega$ is set to be a symplectic structure on $M$ if $\omega$ is closed ($d\omega = 0$) and non-degenerate.
\end{Definition}

For any $x\in M\pau \omega|_x$ is a bilinear skew-symmetric form on $T_xM$. Non-degeneracy means $\forall\ u\ne 0\pau \exists\ v\colon \omega(u,v)\ne 0$. $(M, \omega)$ is a symplectic manifold. Let $H\colon M\to \R$ be a Hamiltonian function. We want to define the Hamiltonian vector field $v_H$.

Consider the differential $dH$. This is a $1$-form on $M$. By definition
\begin{equation}
  \label{vh-diff}
  d H(\cdot) = \omega(\cdot, v_H)
\end{equation}
We occupied the second vector argument of $\omega$ by $v_h$.

Is it possible to compute $v_h$ from this equation? Do we have existence and uniqueness of $v_h$?

\begin{Proposition}
  $v_h$ satisfying \eqref{vh-diff} exists and unique.
\end{Proposition}

\begin{Proof}
  In coordinates for any point $x\in M$ form $\omega$ is determined by a non-degenerate skew-symmetrical matrix $\Omega$ so that $\omega(u, v) = u^T\Omega v$. And in coordinates $dH$ will be a co-vector of the form $\nabla H = (\partial_1 H,\ldots, \partial_m H)$. So \eqref{vh-diff} can be represented by next formula
  \[
    \forall\ v\pau \nabla H\cdot v = v^T\Omega v_H = -v_H^T \Omega v.
  \]
  This means that we can compute $v_H$ from this equation
  \[
    v_H^T = -\nabla H \Omega^{-1}.
  \]
  So, solution exists and unique.
\end{Proof}

\begin{Theorem}
  [Darboux]
  \label{darboux}
  Locally near any point $x\in M$ exist coordinates
  \[(q_1,\ldots, q_k, p_1, \ldots, p_k)\]
  on $M$ such that
  \[
    \omega = \sum\limits_{j=1}^k dp_j\wedge dq_j = dp\wedge dq.
  \]
\end{Theorem}

Corollary: $\dim M = 2k$ is even.

Problem for you: check that in canonical coordinates
\[
    v_H^T = \begin{pmatrix}
      \frac{\partial H}{\partial p_1},
      \ldots,
      \frac{\partial H}{\partial p_k},
      -\frac{\partial H}{\partial q_1},
      \ldots,
      -\frac{\partial H}{\partial q_k},
    \end{pmatrix}.
\]

We now have Hamiltonian equations
\[
  \dot x = v_H(x).
\]

\begin{Proposition}
  H is a first integral of $\dot x = v_H(x)$.
\end{Proposition}
\begin{Proof}
  Let's calculate $L_{v_H}H$. We call it lie derivative or derivative of $H$ along the $v_H$.

  For differential form $\mu$ we have Cartan's formula
  \[
    L_v\mu = d(i_v\mu) + i_v d\mu,
  \]
  where $i_v$ is the substitution of $v$ into $\mu$ as the first argument.
  $H$ is a $0$-form and has no argument. So $i_{v_H}H = 0$ by the definition.

  So $L_{v_H}H = i_{v_H} dH = dH(v_H) = \omega(v_H, v_H) = 0$ by skew-symmetry.
\end{Proof}
\begin{Proposition}
  The flow of Hamilton equations preserves $\omega$.
\end{Proposition}
\begin{Proof}
  $L_{v_H}\omega = d(i_{v_H}\omega) + i_{v_H}\underbrace{d\omega}_{0} = d\big(\underbrace{\omega(v_H, \cdot)}_{-dH}\big) = 0$.
\end{Proof}

\subsection{Poisson bracket}
\begin{Definition}
  For any $F, H \colon M\to \R$ where $(M, \omega)$ is symplectic manifold
  \[
    \{H, F\} = L_{v_H}F = dF(v_H).
  \]
\end{Definition}

Properties.
\begin{enumerate}
  \item $F$ is a first integral of Hamiltonian system with Hamilton function $H$ if and only if $\{H, F\} = 0$.
  \item $\{H,F\} = \omega(v_H, v_F)$.
    \begin{Proof}
      By definition of $v_F$ we have $dF(\cdot) = \omega(\cdot, v_F)$.
      So put there $v_H$.
    \end{Proof}
  \item $\{,\}$ is bi-linear and skew-symmetric. It follows from previous property.
  \item In canonical coordinates we obtain the formula from the textbook
    \[
      \{H, F\} = \sum\limits_{j=1}^k \left(
      \frac{\partial H}{\partial p_j}
      \frac{\partial F}{\partial q_j}
      -
      \frac{\partial H}{\partial q_j}
      \frac{\partial F}{\partial p_j}
      \right).
    \]
    Problem for you is to check.

  \item Leibniz identity
    \[
      \forall\ F,G,H\colon M\to \R\pau
      \{FG, H\} = F\{G,H\} + G\{F,H\}.
    \]
  \item Jacobi identity
    \[
      \big\{F, \{G, H\}\big\}
      +
      \big\{G, \{H, F\}\big\}
      +
      \big\{H, \{F, G\}\big\}
      = 0.
    \]
\end{enumerate}
One of the simplest possibilities to prove last two properties is to check in canonical coordinates.

How to present Hamiltonian vector field if we have only Poisson bracket, but not symplectic structure? The vector field $v_H$ can be defined in terms of $\{,\}$. We define $L_{v_H}$. By definition
\[
  \forall\ F\colon M\to \R\pau L_{v_H}F = \{H, F\}.
\]
So Hamiltonian equation have the next form in local coordinates
\[
  \dot x_1 = \{H, x_1\}, \dots, \dot x_m = \{H, x_m\}.
\]

Another important statement about Poisson bracket.
\begin{Definition}
  A vector space $\mathcal L$ with a bilinear skew-symmetric operation $[\cdot, \cdot]$, which satisfies the Jacobi identity is said to be a Lie algebra.
\end{Definition}
Examples
\begin{enumerate}
  \item $\mathcal L = L(n, \R)$. It is real $n\times n$ matrices, where
    \[
      [A,B] = AB - BA.
    \]
  \item $\mathcal L$ is a space of smooth vector fields on $M$. $[,]$ is the vector commutator. On of possible definitions is
    \[
      L_{[u,v]} = L_uL_v - L_vL_u.
    \]
    Here all second derivatives vanish and this is again an operator.
  \item $\mathcal L$ is the space of functions on $(M,\omega)$ and $[{,}] = \{ {,}\}$.
\end{enumerate}
\begin{Proposition}
  If we take $F,H\colon M\to\R$, then $[v_F, v_H] = v_{\{F,H\}}$.
\end{Proposition}
Corollary. The map $H\mapsto v_H$ is homomorphism of the Lie algebras. This means that this map is linear and transforms commutator to commutator.
\begin{Proof}
  Consider the Lie derivative $L_{v{\{H,F\}}}$. For any function $\phi$ apply it.
  \begin{multline*}
    L_{V_{\{H,F\}}}\phi = \big\{\{H, F\}, \phi\big\} =
    \big\{H, \{ F,\phi\}\big\} - \big\{F, \{H, \phi\}\big\}=\\=
    L_{v_H}L_{v_F}\phi - L_{v_F}L_{v_H}\phi = L_{[v_H, v_f]}\phi.
  \end{multline*}
  Here we used definition of Poisson bracket, Jacobi identity and skew-symmetry of Poisson bracket, then again definition of Poisson bracket and finally the definition of the commutator.
\end{Proof}

\begin{Proposition}
  [Poisson]
  If $F_1, F_2$ are first integrals of the Hamiltonian system with Hamiltonian function $H$, then $\{F_1, F_2\}$ is also a first integral.
\end{Proposition}
\begin{Proof}
  Use the Jacobi identity.
\end{Proof}

Often $\{F_1, F_2\} = 0$ or $\big\{F_1, F_2,\{F_1, F_2\}\big\}$ are interdependent.

More general definition of the Poisson bracket.
\begin{Definition}
  Poisson bracket is a bilinear skew-symmetric operation on $C^\infty(M)$ which satisfies the Leibniz and Jacobi identities.
\end{Definition}
This bracket can be degenerate.

Example. The rigid body with a fixed point in the gravity field.
\begin{figure}[H]
  \centering
  \tpximg{rigitBody}
  \caption{Rigid body}
  \label{fig:rigit-body}
\end{figure}

Here $C$ is a mass center and $I$ is an inertial tensor. We have Eiler equations
\[
    I\dot\omega+\omega\times I\omega = \overline{OC}\times(-mg e_z).
\]

We have also Poisson equations $\dot e_z + \omega\times e_z = 0$.

In coordinates 
\begin{gather*}
  I = \diag(A, B, C),\quad \omega = (p, q, r)^T, \\
  \overline{OC} = (\xi_0, \eta_0, \zeta_0)^T,\quad
  e_z = (\gamma_\xi, \gamma_\eta, \gamma_\zeta)^T.
\end{gather*}
Angular momentum is $(m_\xi, m_\eta, m_\zeta)^T = (Ap, Bq, Cr)^T$. Let $\mu$ be $\mu = mg$.
\begin{eqnarray*}
  A\dot p &=&  (B-C) qr - \mu(\eta_0 \gamma_\zeta - \zeta_0 \gamma_\eta);\\
  B\dot q &=&  (C-A) rp - \mu(\zeta_0 \gamma_\xi - \xi_0 \gamma_\zeta);\\
  C\dot r &=&  (A-B) pq - \mu(\xi_0 \gamma_\eta - \eta_0 \gamma_\xi);\\
  \dot \gamma_\xi &=& -q \gamma_\zeta + r \gamma_\eta;\\
  \dot \gamma_\eta &=& -r \gamma_\xi + p \gamma_\zeta;\\
  \dot \gamma_\zeta &=& -p \gamma_\eta + q \gamma_\xi;\\
\end{eqnarray*}

Problem: check that in the variables $(m_\xi, m_\eta, m_\zeta, \gamma_\xi, \gamma_\eta, \gamma_\zeta)$ these equations are Hamiltonian with Hamiltonian function
\[
  H = \frac12 \left( \frac{m_\xi^2}A + \frac{m_\eta^2}{B} + \frac{m_\zeta^2}{C} \right) +
  \mu(\xi_0\gamma_\xi + \eta_0\gamma_\eta + \zeta_0\gamma_\zeta)
\]
with respect to Poisson bracket $\{ {,}\}$ such that
\begin{gather*}
  \{m_\xi, m_\eta\} = m_\zeta,\pau
  \{m_\eta, m_\zeta\} = m_\xi,\pau
  \{m_\zeta, m_\xi\} = m_\eta,\\
  \{m_\xi, \gamma_\eta\} = \gamma_\zeta,\pau
  \{m_\eta, \gamma_\zeta\} = \gamma_\xi,\pau
  \{m_\zeta, \gamma_\xi\} = \gamma_\eta,\\
  \{m_\xi, \gamma_\zeta\} = -\gamma_\eta,\pau
  \{m_\eta, \gamma_\xi\} = -\gamma_\zeta,\pau
  \{m_\zeta, \gamma_\eta\} = -\gamma_\xi.
\end{gather*}

All other brackets of coordinate function vanish or can be obtained from these by skew-symmetry.

This bracket is degenerate. Indeed, the functions
\[
  m_\xi\gamma_\xi + m_\eta \gamma_\eta + m_\zeta \gamma_\zeta,\quad
  \gamma_\xi^2 + \gamma_\eta^2 + \gamma_\zeta^2
\]
are first integrals, but these functions have zero Poisson brackets with any function.
